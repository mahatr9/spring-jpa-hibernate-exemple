package com.techprimers.jpa.springjpahibernateexemple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaRepositories(basePackages = "com.techprimers.jpa.springjpahibernateexemple.resource")
//@SpringBootApplication(scanBasePackages="com.techprimers.jpa")
@SpringBootApplication
public class SpringJpaHibernateExempleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaHibernateExempleApplication.class, args);
	}

}
