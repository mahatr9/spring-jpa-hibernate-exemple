package com.techprimers.jpa.springjpahibernateexemple.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techprimers.jpa.springjpahibernateexemple.model.UsersLog2;

public interface UsersLog2Repository extends JpaRepository<UsersLog2, Integer> {

}
