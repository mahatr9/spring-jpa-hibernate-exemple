package com.techprimers.jpa.springjpahibernateexemple.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.techprimers.jpa.springjpahibernateexemple.model.Users2;


public interface Users2Repository extends JpaRepository<Users2, Integer> {
	List<Users2> findByName(String name);
	
	@Query(value = "SELECT u FROM Users2 u WHERE u.id = :id")
	public List<Users2> search(@Param("id") Integer id);
}
