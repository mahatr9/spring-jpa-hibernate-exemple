package com.techprimers.jpa.springjpahibernateexemple.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techprimers.jpa.springjpahibernateexemple.model.UsersContact;

public interface UsersContactRepository extends JpaRepository<UsersContact, Integer> {

}
