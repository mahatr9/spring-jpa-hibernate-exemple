package com.techprimers.jpa.springjpahibernateexemple.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.techprimers.jpa.springjpahibernateexemple.model.Users;


public interface UsersRepository extends JpaRepository<Users, Integer> {
	List<Users> findByName(String name);
	
	@Query(value = "SELECT u FROM Users u WHERE u.id = :id")
	public List<Users> search(@Param("id") Integer id);
}
