package com.techprimers.jpa.springjpahibernateexemple.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users2", catalog = "public")
//@Table(name = "users2")
public class Users2 {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private Integer id;
	@Column(name = "name")
	private String name;
	private Integer salary;
	private String teamName;
	
	public Users2() {
	}
	
	public Integer getId() {
		return id;
	}
	public Users2 setId(Integer id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public Users2 setName(String name) {
		this.name = name;
		return this;
	}
	public Integer getSalary() {
		return salary;
	}
	public Users2 setSalary(Integer salary) {
		this.salary = salary;
		return this;
	}
	public String getTeamName() {
		return teamName;
	}
	public Users2 setTeamName(String teamName) {
		this.teamName = teamName;
		return this;
	}
	
}
