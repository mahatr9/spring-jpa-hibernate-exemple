package com.techprimers.jpa.springjpahibernateexemple.resource;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techprimers.jpa.springjpahibernateexemple.model.Users2;
import com.techprimers.jpa.springjpahibernateexemple.model.UsersLog2;
import com.techprimers.jpa.springjpahibernateexemple.repository.UsersLog2Repository;

@RestController
@RequestMapping("/rest/users2/log2")
public class UsersLog2Resource {
	private UsersLog2Repository usersLog2Repository;

	public UsersLog2Resource(UsersLog2Repository usersLog2Repository) {
		this.usersLog2Repository = usersLog2Repository;
	}
	
	@GetMapping(value="/all")
	public List<UsersLog2> getAll() {
		return usersLog2Repository.findAll();
	}
	
	@GetMapping(value="/update/{name}")
	public List<UsersLog2> update(@PathVariable final String name) {
		Users2 users = new Users2();
		
		users.setName(name)
				.setSalary(12000)
				.setTeamName("Operations");
		
		UsersLog2 usersLog1 = new UsersLog2();
		usersLog1.setUsers(users);
		usersLog1.setLog("Hi Youtube");
		usersLog2Repository.save(usersLog1);
		
		UsersLog2 usersLog2 = new UsersLog2();
		usersLog2.setUsers(users);
		usersLog2.setLog("Hi Viewers");
		usersLog2Repository.save(usersLog2);
		
		return usersLog2Repository.findAll();
	}
}
