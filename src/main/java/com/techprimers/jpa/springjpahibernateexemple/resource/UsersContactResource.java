package com.techprimers.jpa.springjpahibernateexemple.resource;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techprimers.jpa.springjpahibernateexemple.model.Users;
import com.techprimers.jpa.springjpahibernateexemple.model.UsersContact;
import com.techprimers.jpa.springjpahibernateexemple.model.UsersLog;
import com.techprimers.jpa.springjpahibernateexemple.repository.UsersContactRepository;

@RequestMapping("/rest/userscontact")
@RestController
public class UsersContactResource {
	private UsersContactRepository usersContactRepository;
	
	public UsersContactResource(UsersContactRepository usersContactRepository) {
		this.usersContactRepository = usersContactRepository;
	}

	@GetMapping(value = "/all")
	public List<UsersContact> getUsersContact() {
		return usersContactRepository.findAll();
	}
	
	@GetMapping(value = "/update/{name}")
	public List<UsersContact> update(@PathVariable final String name) {
		UsersContact usersContact = new UsersContact();
		Users users = new Users();
		UsersLog usersLog = new UsersLog();
		usersLog.setLog("Hi Youtube");
		
		UsersLog usersLog2 = new UsersLog();
		usersLog2.setLog("Hi Viewers");
		
		users.setTeamName("Development")
			.setSalary(10000)
			.setName(name)
			.setUsersLogs(Arrays.asList(usersLog, usersLog2));
		
		usersContact.setPhoneNo(11111)
			.setUsers(users) ;
		
		usersContactRepository.save(usersContact);
		
		return usersContactRepository.findAll();
	}
}
