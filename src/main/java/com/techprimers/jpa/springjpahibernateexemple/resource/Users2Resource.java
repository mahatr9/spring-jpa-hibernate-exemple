package com.techprimers.jpa.springjpahibernateexemple.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techprimers.jpa.springjpahibernateexemple.model.Users2;
import com.techprimers.jpa.springjpahibernateexemple.repository.Users2Repository;

@RestController
@RequestMapping("/rest/users2")
public class Users2Resource {

	@Autowired
	Users2Repository usersRepository;
	
	@GetMapping("/all")
	public List<Users2> getAll() {
		return usersRepository.findAll();
	}
	
	@GetMapping("/{name}")
	public List<Users2> getUser(@PathVariable("name") final String name) {
		return usersRepository.findByName(name);
	}
	
	/*@GetMapping("/id/{id}")
	public List<Users2> getId(@PathVariable("id") final Integer id) {
		
		return usersRepository.search(id);
	}*/
	
	@GetMapping("/id/{id}")
	public Users2 getId(@PathVariable("id") final Integer id) {
		return usersRepository.findById(id).orElse(new Users2());
	}
	
	@GetMapping("/update/{id}/{name}")
	public Users2 update(@PathVariable("id") final Integer id, @PathVariable("name") final String name) {
		Users2 users = getId(id);
		users.setName(name);
		
		return usersRepository.save(users);
	}

}
